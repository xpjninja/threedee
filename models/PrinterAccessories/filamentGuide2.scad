// filament guide Alfawise EX8

// position of mounthing holder:
// left == true - on the left side
// left == false - on the right side

left = true;
// mounting with hole

if (left) {
    translate([-8,0,0]) {
        holder(left=true);
    }
} else {
    translate([8,0,0]) {
        holder(left=false);
    }
}

module holder(left=true) {
    difference() {
    minkowski($fn=50) {
        cube([20,0.5,10]);
        cylinder(r=1,h=1);
    }
    if (left) {
        translate([5,-2,5]){ rotate([0,90,90]){cylinder(h=5,r=1.5, $fn=60);}}   
    } else {
        translate([15,-2,5]){ rotate([0,90,90]){cylinder(h=5,r=1.5, $fn=60);}}
    }
}
}

translate([7.5,0,0]) {
    difference() {
        cube([5,17,5]);
        translate([2.5,18,5]) {
           sphere(7);
       }
    }
}


// filament guide with cleaner
difference() {
    translate([10,22,0]) {
        rounded_cylinder_with_bottom(ch=20,cr=12,ct=1.5);
        translate([0,0,15]) { rounded_cylinder(ch=10, cr=11, ct=1); }
    }
    translate([10,22,0]) {cylinder(4,1,1, $fn=60);}
}


// filament guide cap
difference() {
    translate([35,22,0]) {
                rounded_cylinder_with_bottom(ch=10,cr=12,ct=1);

//        translate([0,0,5]) { rounded_cylinder(ch=5, cr=12, ct=1); }
    }
    translate([35,22,0]) {cylinder(4,1,1, $fn=60);}
}

module rounded_cylinder(ch=5,cr=10,ct=1, smooth=60) {
    difference() {
			cylinder(ch,cr,cr,center=false,$fn=smooth);
			cylinder(ch,cr-ct,cr-ct,center=false,$fn=smooth);
    }
}

// Taken from:
// https://www.thingiverse.com/thing:6455
// http://www.iheartrobotics.com/2011/02/openscad-tip-round-2-of-3.html
module rounded_cylinder_with_bottom(ch=20, cr=10,ct=1,r=4,smooth=60) {
    
    pad = 0.1;	// Padding to maintain manifold
    
    difference() {
	union() {
		// Basic cup shape
		difference() {
			cylinder(ch,cr,cr,center=false,$fn=smooth);
			translate([0,0,ct])
				cylinder(ch-ct+pad,cr-ct,cr-ct,center=false,$fn=smooth);
		}        
		// Inside Fillet
		difference() {
			rotate_extrude(convexity=10,  $fn = smooth)
				translate([cr-ct-r+pad,ct-pad,0])
					square(r+pad,r+pad);
			rotate_extrude(convexity=10,  $fn = smooth)
				translate([cr-ct-r,ct+r,0])
					circle(r=r,$fn=smooth);
		}
	}		
	
	// Bottom Round
	difference() {
		rotate_extrude(convexity=10,  $fn = smooth)
			translate([cr-r+pad,-pad,0])
				square(r+pad,r+pad);
		rotate_extrude(convexity=10,  $fn = smooth)
			translate([cr-r,r,0])
				circle(r=r,$fn=smooth);
	}
}
}
