// back wires guide

difference() {
    union(){ 
        translate([-4,5,9]) rotate([90,0,0]) {
            linear_extrude(5) circle(r=9);
        }
        translate([-5,0,0]) cube([19,5,18]);
    }
translate([-4,5,9]) rotate([90,0,0]) {linear_extrude(5) circle(r=4.5);}
}

translate([6,0,0]) cube([8,20,8]);
translate([8,0,8]) cube([4,20,8]);

translate([-13,80,0]) cube([8,20,8]);
translate([-11,80,8]) cube([4,20,8]);

linear_extrude(8) polygon([[-13,80], [-5,80], [14,20], [6,20]]);
translate([0,0,8]) linear_extrude(8) polygon([[-11,80], [-7,80], [12,20], [8,20]]);

translate([-13,92,0]) cube([8,8,40]);
translate([-9,96,50]) mirror([0,0,1]) rotate([90,0,0]) difference() {
    rotate_extrude() translate([10, 0, 0]) circle(r = 4, $fn = 50);
    translate([-5,-20,-4]) cube([10,20,8]);
}


