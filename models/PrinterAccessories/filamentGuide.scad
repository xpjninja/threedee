// filament guide Alfawise EX8




// mounting with hole
difference() {
    minkowski(){
        cube([20,0.5,15]);
        cylinder(r=1,h=1); 
    }
  
  translate([10,-2,10]){
    rotate([0,90,90]){cylinder(h=4,r=1.5);}
    }
   
}


translate([7.5,0,0]) {
   cube([5,17,5]);
}

// filament guide 
difference() {
translate([10,22,0]) scale([1,1.4,1])oval(12,10,5);

translate([7,16,0])
    rotate([0,0,135])
        cube([4,1,100]);
}

$fn = 16;
module circle3d(r1=10, r2=8, h=5) {
    linear_extrude(h) {
        difference() {            
          offset(r=2) circle(r1);
          circle(r2);
        }
     }
}

module oval(r1=10, r2=8, h=5){
    scale([1.7,0.3,1])circle3d(r1,r2,h); 
}

