// box width
bw = 110;
// box depth
bd = 110;
// bottom box height
bbh = 30;
// top box height
bth = 10;
// wall width
ww = 1.5;


module noha(x,y) {
    translate([x,y,0]) {
        difference() {
        translate([0,0,0]) { cylinder(h=10,r=5); }
        translate([0,0,5]) { cylinder(h=5, r=1.5); }
        }
    }    
}

module audioout(x,y) {
    translate([x,y,0]) {
        
        translate([0,0,20]) {rotate(a=90, v=[1,0,0]) {cylinder(h=ww, r=2); }}
        translate([45,0,20]) {rotate(a=90, v=[1,0,0]) {cylinder(h=ww, r=2);}}
        translate([10,-ww,10]) { cube([6,15,3]);}
        translate([30,-ww,10]) { cube([6,15,3]);}
    }
    
}

module engrave(x,y,txt,size=5) {
    translate ([x,y,0]) {
       linear_extrude(height = 1) {
       text(txt, font = "Liberation Sans", size = size);
     }
 }
}


module krabice() {
    union() {
    difference() {
        cube(size=[bw + 2 * ww, bd + 2 * ww, bbh]);
        translate([ww,ww,ww]) { cube(size=[bw,bd,bbh - ww]);}
        
         for (i=[0:7]) {
            translate([20, 20 + i * 10, 0]) {cube(size=[bw - 45, 5, ww]);}
         }
            // volume
            translate([79, ww, 18.5]) {
                rotate(a=90, v=[1,0,0]) {
                        cylinder(h=ww, r=4);
                        translate([0,0,1]){ engrave(-6,-15.5, "VOL");}
                    }
            }
            // aux
            translate([54, ww, 15]) {
                rotate(a=90, v=[1,0,0]) {
                    cylinder(h=ww, r=6);
                   translate([0,0,1]){ engrave(-7,-12, "AUX");}
                }
            }
            // CF
            translate([17, 0, 12]) {cube(size=[20,2*ww,4]);}
            translate([17, 0.5, 12]) {
                rotate(a=90, v=[1,0,0]) {engrave(6,-9, "CF");}
                
            }
            // power
            translate([0, 100, 20]) {
                rotate(a=90, v=[0,1,0]) {cylinder(h=ww, r=5);}
            }
            // power button
            translate([bw+ww, 88, 25]) {
                
                rotate(a=90, v=[0,1,0]) { cube(size=[13,19,2*ww]);}
            }
            // audio out
            audioout(5,bd + 2*ww);
            audioout(62,bd + 2*ww);
            
            translate([bd-ww,0.5,2*ww]) {
            rotate(a=90, v=[1,0,0]) {rotate(90){engrave(0,0, "XPJ",9);}}
          }
    }

    // nohy
    noha(10,5);
    noha(95.5,5);
    noha(10,83.5);
    noha(95.5,83.5);
    }
}
module krabiceTop() {
  union() {  
    difference() {
        translate([ww,ww,bbh-5]) {cube(size=[bw,bd,10]);}
        translate([ww+1,ww+1,bbh-5]) {cube(size=[bw-2,bd-2,10]);}
    }
    translate([0,0,bbh]) {
        difference() {
            cube(size=[bw + 2 * ww, bd + 2 * ww, bth]);
            translate([ww,ww,0]) { cube(size=[bw,bd,bth - ww]);}
            for (i=[0:7]) {
            translate([20, 20 + i * 10, bth-ww]) {cube(size=[bw - 36, 5, ww]);}
         }
        }
    }
  }  
}

union() {
    //krabice();
    krabiceTop();
}    



