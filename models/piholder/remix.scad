union() {
import("boite_raspberry.stl", convexity=10);

translate([-28,0,0]) {cube([56,1,30]);}
difference() {
  translate([-5, 0, -58]) {cube(size=[10,1,135]);}
  
  // hole on left
  rotate([0,90,90]) { translate([53, 0, 0]) { cylinder(h=2, r=2);}}
  // hole on right
  rotate([0,90,90]) { translate([-72, 0, 0]) { cylinder(h=2, r=2);}}

}
}