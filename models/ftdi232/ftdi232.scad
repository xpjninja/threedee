// FTDI232 Case
translate([50,39,0]) rotate([0,0,180]) top();
bottom();

module bottom() {
    difference() {
        $fn = 50;
        translate([1,1,0]) minkowski(50) {
            cube([19,37,10]); //outer
            cylinder(1);
        }
            
        
        translate([1,1,2]) cube([19,37,10]); // inner
        translate([2,0,6]) cube([17,2,6]); // pins
        translate([6,38,3]) cube([9,2,5]); // usb
        
        translate([16,4,0])  render_text("GND");
        translate([17,0,0])  cube([1.5,4,1]);
        
        translate([14,19,0]) render_text("CTS");
        translate([14,0,0])  cube([1.5,18,1]);
        
        translate([19,34,0]) rotate([0,0,90]) render_text("VCC");
        translate([11,0,0])  cube([1.5,33,1]);
        
        translate([5,25,0])  render_text("TX");
        translate([8,0,0])  cube([1.5,25,1]);
        
        translate([2,17,0])  render_text("RX");
        translate([5,0,0])  cube([1.5,16,1]);
        
        translate([0,3,0])  render_text("DTR");    
        translate([2,0,0])  cube([1.5,3,1]);    
    }
}

module top() {
    
    difference() {
        union() {
            $fn=50; translate([1,1,0]) minkowski(50) {
                    cube([19,37,1]); //outer
                    cylinder(1);
            }
            translate([4,12,2]) cube([8,8,4]);
            
            translate([3.5,3.5,2]) cylinder(h=4, r=2.5);
            
                translate([1,1,2]) cube([19,2,3]);
                translate([1,6,2]) cube([2,28,2]);    
                translate([18,6,2]) cube([2,28,2]);
                translate([18,29,2]) cube([2,4,6]);
                translate([1,29,2]) cube([2,4,6]);
        }
        
        translate([5,30,0]) cube([11,5,4]);
        translate([6,29,0]) rotate([0,0,180]) render_text("5V");
        translate([20,29,0]) rotate([0,0,180]) render_text("3.3V");
        
        translate([3,10,0]) rotate([0,0,-90]) render_text("FTDI");
        
        translate([5,13,1]) cube([6,6,6]);
        $fn=50; translate([3.5,3.5,1]) cylinder(h=5,r=1.5);
    }
    

    
}

module render_text(text="") {
    mirror([0,1,0]) 
        rotate([0,0,-90]) 
            linear_extrude(1) 
                text(text = text, size = 5, font = "Liberation Sans:style=Bold");
}