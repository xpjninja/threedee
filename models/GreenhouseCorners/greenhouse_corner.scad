difference() {
union() {
    // bottom
    translate([0,5,0]){cube(size=[50,50,5]);}
     
    
    // top y
    rotate(a=-90, v=[0,1,0]) {cube(size=[100,40,5]);}
    translate([15,20,0]) {rotate(a=-90, v=[0,1,0]) {cube(size=[100,20,5]);}}

    // top x
    translate([0,5,0]) {rotate(a=90,v=[1,0,0]) {cube(size=[35,100,5]);}}
    translate([10,20,0]) {rotate(a=90,v=[1,0,0]) {cube(size=[25,100,5]);}}
    
    // botom y
    cube(size=[10,55,5]);
    rotate(a=-90, v=[0,1,0]) {cube(size=[25,55,5]);}
    translate([15,15,0]) {rotate(a=-90, v=[0,1,0]) {cube(size=[25,40,5]);}}
    
    
    translate([150,5,0]){
        rotate(a=90,v=[0,0,1]) {
            translate([0,100,0]) {
                cube(size=[10,50,5]);
                rotate(a=-90, v=[0,1,0]) {cube(size=[25,50,5]);}
                translate([15,0,0]) {rotate(a=-90, v=[0,1,0]) {cube(size=[25,40,5]);}}
            }}}
    

    } // end union
    
    // bottom hole
    translate([27,32,0]) {cylinder(h=10, r=3.5, center=true, $fn=100);}
    // bottom cut
    translate([50,20,0]){rotate(a=45,v=[0,0,1]) {cube([30,49,10]);}}

    // x
    translate([42,0,18]) {
        rotate(a=90,v=[1,0,0]) {cylinder(h=40, r=3, center=true, $fn=100);}}
    translate([90,0,15]) {
        rotate(a=90,v=[1,0,0]) {cylinder(h=40, r=3, center=true, $fn=100);}} 
    translate([25,0,90]) {
        rotate(a=90,v=[1,0,0]) {cylinder(h=40, r=3, center=true, $fn=100);}}        
    // y
    rotate(a=90,v=[0,0,1]) {   
    translate([45,0,18]) {
        rotate(a=90,v=[1,0,0]) {cylinder(h=40, r=3, center=true, $fn=100);}}  
    translate([90,0,15]) {
        rotate(a=90,v=[1,0,0]) {cylinder(h=40, r=3, center=true, $fn=100);}}    
    translate([30,0,80]) {
        rotate(a=90,v=[1,0,0]) {cylinder(h=40, r=3, center=true, $fn=100);}}
    }
}